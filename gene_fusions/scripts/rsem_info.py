import os

import info
import utils


install_directory = os.path.join(info.install_directory, 'rsem')

packages_directory = os.path.join(install_directory, 'packages')
bin_directory = os.path.join(install_directory, 'bin')
data_directory = os.path.join(install_directory, 'data')


import glob
import shutil
import os
import sys
import subprocess

import utils
import info
import defuse_info


Sentinal = utils.Sentinal(os.path.join(defuse_info.install_directory, 'sentinal_'))


with Sentinal('install') as sentinal:

    if sentinal.unfinished:

        utils.rmtree(defuse_info.packages_directory)
        utils.makedirs(defuse_info.packages_directory)

        utils.makedirs(defuse_info.data_directory)

        with utils.CurrentDirectory(defuse_info.packages_directory):

            subprocess.check_call('git clone https://dranew@bitbucket.org/dranew/defuse.git'.split(' '))

            with utils.CurrentDirectory(os.path.join(defuse_info.defuse_directory, 'tools')):

                subprocess.check_call(['make'])

            template_config_filename = os.path.join(defuse_info.defuse_directory, 'scripts', 'config.txt')

            with open(defuse_info.config_filename, 'w') as config_file, open(template_config_filename, 'r') as template_config_file:

                for line in template_config_file:
                    if not line.startswith('#') and '=' in line:
                        key, value = line.split('=')
                        key = key.strip()
                        value = value.strip()

                        if key == 'source_directory':
                            line = 'source_directory = {0}\n'.format(defuse_info.defuse_directory)
                        elif key == 'dataset_directory':
                            line = 'dataset_directory = {0}\n'.format(defuse_info.data_directory)
                        elif key == 'chromosomes':
                            line = 'chromosomes = {0}\n'.format(','.join(info.chromosomes))
                        elif '[path of your' in value:
                            line = '{0} = {1}\n'.format(key, value.split(' ')[3])
                    
                    config_file.write(line)
                    

with Sentinal('createref') as sentinal:

    if sentinal.unfinished:

        with utils.CurrentDirectory(defuse_info.packages_directory):

            subprocess.check_call([defuse_info.createref_script, '-c', defuse_info.config_filename])

